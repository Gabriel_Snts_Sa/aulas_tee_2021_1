// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBsUeRqQcBChyI0YbmsTUusIXBSLsnweow',
    authDomain: 'controle-if1.firebaseapp.com',
    projectId: 'controle-if1',
    storageBucket: 'controle-if1.appspot.com',
    messagingSenderId: '152598206790',
    appId: '1:152598206790:web:c738dec19ebf963333ed47',
    measurementId: 'G-5L55ZQ4F06'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
