import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  divide(dividendo, divisor) {
    if(divisor === 0) {
      return "Não existe divisão por Zero!";
    }
    return dividendo / divisor;
  }
}