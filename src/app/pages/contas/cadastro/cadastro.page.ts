import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { DateHelper } from 'src/app/helpers/DateHelper';
import { ContaService } from './../service/contas.service'

@Component({
  selector: 'cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  contasForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private conta: ContaService,
    private nav: NavController,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.contasForm = this.builder.group({
      date:       [new Date().toISOString(), Validators.required],
      valor:      ['', [Validators.required, Validators.min(0.01)]],
      descricao:  ['', [Validators.required, Validators.minLength(5)]],
      parceiro:   ['', [Validators.required, Validators.minLength(6)]],
      tipo:       ['', [Validators.required]],
    });
  }

  // Salva a nova conta no Firebase
  registraConta() {
    let conta = this.contasForm.value;
    const date = this.contasForm.get('date').value;
    conta = {...conta, ...DateHelper.breakDate(date)};
    delete conta.date;

    this.conta.registraConta(conta)
    .then(() => this.nav.navigateForward(`contas/${conta.tipo}`));
  }
}
